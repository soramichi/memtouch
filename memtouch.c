#include <stdlib.h>
#include <stdio.h>
//#include <time.h>
#include <sys/time.h>
#include <unistd.h>

#define PAGE_SIZE 4096
#define PAGE_SIZE_IN_BITS (4096*8)

// change the value of the `n'th bit in `byte'
void dirty_a_bit(char* byte, int n){
  int val = *byte;
  *byte = (val ^ (1<<n));
}

// ./a.out working_set_size pages_per_second [bytes_per_page] [duraion] [bit_mode]
int main(int argc, char* argv[]){
  char* memory;
  size_t working_set_size;
  int pages_per_second, bytes_per_page, bits_per_page, duration, num_pages, bit_mode;
  int i, j, t;
  size_t index = 0, jndex = 0;

  if(argc<3){
    fprintf(stderr, "Usage: %s working_set_size pages_per_second [bytes_per_page / bit_per_second] [duraion] [bit_mode]\n", argv[0]);
    return 1;
  }
  
  working_set_size = (unsigned long)atoi(argv[1]) * 1024 * 1024;
  num_pages = working_set_size / PAGE_SIZE;
  pages_per_second = atoi(argv[2]);

  if(argc<4)
    bytes_per_page = 1;
  else
    bytes_per_page = atoi(argv[3]);

  if(argc<5)
    duration = 180;
  else
    duration = atoi(argv[4]);
  
  if(argc<6)
    bit_mode = 0;
  else{
    bit_mode = !!atoi(argv[5]);
    bits_per_page = bytes_per_page;
  }

  printf("working_set_size: %lu\n", working_set_size);
  printf("pages_per_second: %d\n", pages_per_second);
  printf("bytes_per_page: %d\n", bytes_per_page);
  printf("duration: %d\n", duration);
  printf("bit_mode: %d\n", bit_mode);

  memory = (char*)calloc(num_pages, PAGE_SIZE);

  if(memory == NULL){
    perror("calloc(num_pages, PAGE_SIZE)");
    return 1;
  }

  for(t=0;t<duration;t++){
    struct timeval start, end;
    
    gettimeofday(&start, NULL);

    // index and jndex are NOT initialized to zero in each loop iteration,
    // making sure that the memory is kept being dirty
    for(i=0;i<pages_per_second;i++){
      for(j=0;j<bytes_per_page;j++){
	if(!bit_mode){
	  memory[index * PAGE_SIZE + jndex * (PAGE_SIZE / bytes_per_page)]++;
	}
	else{
	  dirty_a_bit(&memory[(index%num_pages) * PAGE_SIZE], (jndex%bits_per_page) * (PAGE_SIZE_IN_BITS / bits_per_page));
	}

	jndex++;
	if(jndex == bytes_per_page)
	  jndex = 0;
      }

      index++;
      if(index == num_pages)
	index = 0;
    }

    gettimeofday(&end, NULL);

    if(start.tv_sec == end.tv_sec){
      usleep(1000000 - (end.tv_usec - start.tv_usec));
    }
    else if(start.tv_sec + 1 == end.tv_sec && start.tv_usec >= end.tv_usec){
      usleep(1000000 - (1000000 + end.tv_usec - start.tv_usec));
    }
    else{
      fprintf(stderr, "Error: Writing to the working set took more than 1 second!\n");
      return 1;
    }
    
    putchar('#');
    fflush(stdout);
  }

  puts("");

  return 0;
}
